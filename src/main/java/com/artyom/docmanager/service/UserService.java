package com.artyom.docmanager.service;

import com.artyom.docmanager.model.User;

public interface UserService {
    public User getUser(String login);
}
