package com.artyom.docmanager.service;

import com.artyom.docmanager.dao.UserDao;
import com.artyom.docmanager.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    public User getUser(String login) {
        return userDao.getUser(login);
    }

}
