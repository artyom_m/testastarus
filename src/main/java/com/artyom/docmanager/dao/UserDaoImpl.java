package com.artyom.docmanager.dao;

import com.artyom.docmanager.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;


    @SuppressWarnings("unchecked")
    public User getUser(String login){
        List<User> userList = new ArrayList<User>();
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM User u WHERE u.login = :login");
        query.setParameter("login", login);
        userList = query.list();
        if (userList.size() > 0)
        return userList.get(0);
	        else
        return null;
    }
}
